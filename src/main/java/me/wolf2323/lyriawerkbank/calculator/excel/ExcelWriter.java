package me.wolf2323.lyriawerkbank.calculator.excel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FontUnderline;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


class ExcelWriter
{
	final static Path	xls	= Paths.get("LyriaWerkbank.xlsx");
	static CellStyle	headerStyle;
	static CellStyle	headerStyleSub;

	public static XSSFWorkbook loadWorkbook() throws IOException
	{
		XSSFWorkbook workbook;
		if(Files.isRegularFile(xls))
		{
			try(final InputStream file = Files.newInputStream(xls, StandardOpenOption.CREATE, StandardOpenOption.READ))
			{
				workbook = new XSSFWorkbook(file);
			}
		}
		else
		{
			workbook = new XSSFWorkbook();
		}

		headerStyle = loadStyleHeader(workbook);
		headerStyleSub = loadStyleSubHeader(workbook);

		return workbook;
	}

	public static void saveWorkbook(XSSFWorkbook workbook) throws IOException
	{
		final OutputStream outputStr = Files.newOutputStream(xls, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		workbook.write(outputStr);
		outputStr.close();
	}

	private static CellStyle loadStyleHeader(XSSFWorkbook workbook)
	{
		CellStyle headerStyle = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 16);
		font.setBold(true);
		font.setUnderline(FontUnderline.SINGLE);
		headerStyle.setFont(font);
		return headerStyle;
	}

	private static CellStyle loadStyleSubHeader(XSSFWorkbook workbook)
	{
		CellStyle subHeaderStyle = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setUnderline(FontUnderline.SINGLE);
		subHeaderStyle.setFont(font);
		return subHeaderStyle;
	}

	public static class ExcelSheet
	{
		Sheet sheet;

		public ExcelSheet(Sheet sheet)
		{
			this.sheet = sheet;
		}

		public Cell getCell(int row, int cell)
		{
			Row r = sheet.getRow(row);
			if(r != null)
			{
				Cell c = r.getCell(cell);
				if(c != null)
				{
					return c;
				}
				return r.createCell(cell);
			}
			return sheet.createRow(row).createCell(cell);
		}
	}
}
