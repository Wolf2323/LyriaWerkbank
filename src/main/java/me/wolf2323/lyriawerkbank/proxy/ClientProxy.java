package me.wolf2323.lyriawerkbank.proxy;

import org.lwjgl.glfw.GLFW;

import me.wolf2323.lyriawerkbank.importer.RecipeImporter;
import me.wolf2323.lyriawerkbank.inventory.GuiListener;
import me.wolf2323.lyriawerkbank.inventory.factories.CountConstantsIngredientFactorie;
import me.wolf2323.lyriawerkbank.inventory.factories.CountIngredientFactorie;
import me.wolf2323.lyriawerkbank.inventory.factories.ShapedNBTRecipeSerializer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.util.InputMappings;
import net.minecraft.item.crafting.RecipeSerializers;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.client.settings.KeyModifier;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.fml.client.registry.ClientRegistry;


public class ClientProxy extends CommonProxy
{
	private final static KeyConflictContext	CONF_CON					= KeyConflictContext.UNIVERSAL;
	private final static String				CATEGORY					= "LyriaWerkbank";

	public final static KeyBinding			BINDING_AUTOCRAFT			= new KeyBinding("Autocraft", CONF_CON, KeyModifier.CONTROL, InputMappings.Type.KEYSYM,
			GLFW.GLFW_KEY_A, CATEGORY);
	public final static KeyBinding			BINDING_AUTOCRAFT_AND_DROP	= new KeyBinding("Autocraft & Drop", CONF_CON, KeyModifier.CONTROL, InputMappings.Type.KEYSYM,
			GLFW.GLFW_KEY_Q, CATEGORY);
	public final static KeyBinding			BINDING_RESEARCH_WORKBENCH	= new KeyBinding("Workbench Research", CONF_CON, InputMappings.Type.KEYSYM,
			GLFW.GLFW_KEY_LEFT_CONTROL, CATEGORY);

	public void construct()
	{
		super.construct();

		ClientRegistry.registerKeyBinding(BINDING_AUTOCRAFT);
		ClientRegistry.registerKeyBinding(BINDING_AUTOCRAFT_AND_DROP);
		ClientRegistry.registerKeyBinding(BINDING_RESEARCH_WORKBENCH);

		MinecraftForge.EVENT_BUS.register(new RecipeImporter());
		MinecraftForge.EVENT_BUS.register(new GuiListener());

		CraftingHelper.register(new ResourceLocation("lyriawerkbank:count_ingredient"), new CountIngredientFactorie());
		CraftingHelper.register(new ResourceLocation("lyriawerkbank:count_constants_ingredient"), new CountConstantsIngredientFactorie());
		RecipeSerializers.register(new ShapedNBTRecipeSerializer());
	}

	public void setup()
	{
		super.setup();
	}

	public void ready()
	{
		super.ready();
	}
}
