package me.wolf2323.lyriawerkbank.inventory;

import java.lang.reflect.Field;

import cpw.mods.modlauncher.api.INameMappingService;
import me.wolf2323.lyriawerkbank.proxy.ClientProxy;
import me.wolf2323.lyriawerkbank.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.recipebook.GuiRecipeBook;
import net.minecraft.client.gui.recipebook.RecipeBookPage;
import net.minecraft.client.util.InputMappings;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;


public class GuiRecipeBookOverride extends GuiRecipeBook
{
	private static GuiRecipeBookOverride	instace;

	private Field							recipeBookPageField;

	private GuiTextField					seachBar;
	private static String					lastSearch;

	public GuiRecipeBookOverride()
	{
		super();
		try
		{
			recipeBookPageField = GuiRecipeBook.class.getDeclaredField(ObfuscationReflectionHelper.remapName(INameMappingService.Domain.FIELD, "recipeBookPage"));
			Utils.reflectSetFinal(GuiRecipeBook.class.getDeclaredField(ObfuscationReflectionHelper.remapName(INameMappingService.Domain.FIELD, "ghostRecipe")), this,
					new GhostRecipeOverride());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		instace = this;
	}

	public static GuiRecipeBookOverride getGuiRecipeBookOverride()
	{
		return instace;
	}

	@Override
	public void func_201518_a(boolean kp)
	{
		super.func_201518_a(kp);
		try
		{
			Field field = GuiRecipeBook.class.getDeclaredField(ObfuscationReflectionHelper.remapName(INameMappingService.Domain.FIELD, "searchBar"));
			field.setAccessible(true);
			Object o = field.get(this);
			seachBar = (GuiTextField) o;
			if(lastSearch != null && !lastSearch.isEmpty() && InputMappings.isKeyDown(ClientProxy.BINDING_RESEARCH_WORKBENCH.getKey().getKeyCode()))
			{
				seachBar.setText(lastSearch);
			}
		}
		catch(IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		if(CraftingManager.getCraftingManager() != null)
		{
			CraftingManager.getCraftingManager().breakAutoCraftAndDrop();
		}
		boolean orig = super.mouseClicked(mouseX, mouseY, mouseButton);
		if(orig && this.isVisible() && !Minecraft.getInstance().player.isSpectator() && !Minecraft.getInstance().isSingleplayer())
		{
			try
			{
				RecipeBookPage page = (RecipeBookPage) recipeBookPageField.get(this);
				IRecipe recipe = page.getLastClickedRecipe();
				if(recipe != null && recipe.getGroup().startsWith("LyriaWerkbank:"))
				{
					if(!CraftingManager.getCraftingManager().craft(recipe, GuiScreen.isShiftKeyDown()))
					{
						super.setupGhostRecipe(recipe, Minecraft.getInstance().player.openContainer.inventorySlots);
					}
				}

			}
			catch(IllegalArgumentException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}

		if(InputMappings.isKeyDown(ClientProxy.BINDING_AUTOCRAFT.getKey().getKeyCode()))
		{
			CraftingManager.getCraftingManager().autoCraft();
		}

		if(InputMappings.isKeyDown(ClientProxy.BINDING_AUTOCRAFT_AND_DROP.getKey().getKeyCode()))
		{
			CraftingManager.getCraftingManager().autoCraftAndDrop(GuiScreen.isShiftKeyDown());
		}

		return orig;
	}

	@Override
	public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_)
	{
		boolean orig = super.charTyped(p_charTyped_1_, p_charTyped_2_);

		if(orig && seachBar != null && seachBar.isFocused())
		{
			lastSearch = seachBar.getText();
		}
		return orig;
	}
}
