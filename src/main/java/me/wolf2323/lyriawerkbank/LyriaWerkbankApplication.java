package me.wolf2323.lyriawerkbank;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.TreeMap;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import me.wolf2323.lyriawerkbank.calculator.Calculator;
import me.wolf2323.lyriawerkbank.calculator.Recipe;
import me.wolf2323.lyriawerkbank.calculator.excel.ExcelPriceWriter;


@SuppressWarnings("serial")
public class LyriaWerkbankApplication extends JFrame
{
	public static void main(String[] args)
	{
		new LyriaWerkbankApplication();
	}

	private LyriaWerkbankApplication()
	{
		setupWindow();

		// Input
		JTextField textFieldItem = new JTextField(30);
		JPanel panelInput = setupInput(textFieldItem);

		// Output
		DefaultListModel<String> listModelItems = new DefaultListModel<>();
		JList<String> listItems = new JList<>(listModelItems);
		JPanel panelOutput = setupOutput(listModelItems, listItems);

		// Save
		JButton buttonSave = new JButton("Berechnen und Speichern");

		// main container
		Container pane = getContentPane();
		pane.add(panelInput, BorderLayout.BEFORE_FIRST_LINE);
		pane.add(panelOutput);
		pane.add(buttonSave, BorderLayout.SOUTH);

		setVisible(true);

		// Setup Price Table
		try
		{
			ExcelPriceWriter.write();
		}
		catch(IOException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler beim erstellen der Preis Tabelle. \n" + e.getMessage(), "Warnung", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			return;
		}

		// Loading of recipes
		try
		{
			Calculator.loadRecipes();
		}
		catch(IOException | URISyntaxException e)
		{
			JOptionPane.showMessageDialog(null, "Fehler beim laden der Rezepte! \n" + e.getMessage(), "Warnung", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			return;
		}

		// Default Items
		Collection<Recipe> items = new TreeMap<>(Calculator.getRecipes()).values();
		// Add default Items
		for(Recipe s : items)
		{
			listModelItems.addElement(s.getName());
		}

		// Setup Listeners
		setupTextFieldItem(textFieldItem, listModelItems, items);
		setupButtonSave(buttonSave, listItems, textFieldItem);
	}

	private void setupWindow()
	{
		setTitle("LyriaWerkbank Taschenrechner");
		setSize(750, 750);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
	}

	private JPanel setupInput(JTextField textFieldItem)
	{
		JLabel labelItem = new JLabel("Item");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel.add(labelItem, BorderLayout.NORTH);
		panel.add(textFieldItem, BorderLayout.NORTH);

		return panel;
	}

	private JPanel setupOutput(DefaultListModel<String> listModelItems, JList<String> listItems)
	{
		listItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listItems.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		listItems.setVisibleRowCount(-1);
		listItems.setFixedCellWidth(225);
		JScrollPane listScrollerIten = new JScrollPane(listItems);
		listScrollerIten.setPreferredSize(new Dimension(700, 650));

		JPanel panel = new JPanel();
		panel.add(listScrollerIten);

		return panel;
	}

	private void setupTextFieldItem(JTextField textFieldItem, DefaultListModel<String> listModelItems, Collection<Recipe> items)
	{
		textFieldItem.addKeyListener(new KeyListener()
		{

			@Override
			public void keyReleased(KeyEvent e)
			{
				listModelItems.removeAllElements();
				String input = textFieldItem.getText();
				for(Recipe s : items)
				{
					if(input.isEmpty() || s.getName().toLowerCase().contains(input.toLowerCase()))
					{
						listModelItems.addElement(s.getName());
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent e)
			{
			}

			@Override
			public void keyPressed(KeyEvent e)
			{
			}
		});
	}

	private void setupButtonSave(JButton buttonSave, JList<String> listItems, JTextField textFieldItem)
	{

		buttonSave.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				String input = listItems.getSelectedValue();
				if(input == null)
				{
					input = textFieldItem.getText();
				}
				try
				{
					String name = Calculator.calculate(input);
					JOptionPane.showMessageDialog(null, "Das Ergebnis wurde in 'LyriaWerkbank.xlsx' im Tabellenblatt \n'" + name + "' gespeichert", "Info",
							JOptionPane.INFORMATION_MESSAGE);
				}
				catch(Exception ex)
				{
					JOptionPane.showMessageDialog(null, "Fehler beim berechnen der Rezepte! \n" + ex.getMessage(), "Warnung", JOptionPane.WARNING_MESSAGE);
					ex.printStackTrace();
				}
			}
		});
	}
}
