package me.wolf2323.lyriawerkbank.importer;

import java.util.HashMap;
import java.util.Set;

import com.google.gson.JsonObject;

import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.text.TextFormatting;


class RecipeImporterItem
{
	private final String	item;
	private String			name;
	private final int		count;
	private CompoundNBT		nbt;

	public RecipeImporterItem(ChestContainer chest, int slot)
	{
		ItemStack stack = chest.inventorySlots.get(slot).getStack();
		item = stack.getItem().getRegistryName().toString();
		name = stack.hasDisplayName() ? getUnformattedText(stack.getDisplayName().getFormattedText()) : item;
		count = stack.getCount();
		nbt = stack.getTag() == null ? null : stack.getTag().copy();
		clearLyriaDisplayTag();
		fixEnchantments();
	}

	public Character getUnusedKey(HashMap<Character, RecipeImporterItem> keys)
	{
		if(name.equals("minecraft:air"))
		{
			return ' ';
		}
		String keyName = name.startsWith("minecraft:") ? name.substring(10) : name;
		return getRecusiveKey(keys.keySet(), keyName.toUpperCase());
	}

	private Character getRecusiveKey(Set<Character> keySet, String name)
	{
		Character c = name.charAt(0);
		if(keySet.contains(c))
		{
			return getRecusiveKey(keySet, name.substring(1));
		}
		return c;
	}

	private void clearLyriaDisplayTag()
	{
		if(nbt == null || !nbt.contains("display"))
		{
			return;
		}
		CompoundNBT display = nbt.getCompound("display");
		if(!display.contains("Lore"))
		{
			return;
		}
		ListNBT lore = display.getList("Lore", 8);
		if(!lore.getString(lore.size() - 1).contains("-- Anzeigeitem --"))
		{
			return;
		}
		lore.remove(lore.size() - 1);

		// remove an empty line from the Lore
		if(lore.size() != 0)
		{
			lore.remove(lore.size() - 1);
		}
		// delete an empty Lore and display
		if(lore.size() == 0)
		{
			display.remove("Lore");
			if(display.keySet().size() == 0)
			{
				nbt.remove("display");
				if(nbt.keySet().size() == 0)
				{
					nbt = null;
				}
			}
		}
	}

	public String setDataAdditional(ItemStack stack)
	{
		name = getUnformattedText(stack.getDisplayName().getFormattedText()).split(": ")[1];

		ListNBT tagList = stack.getTag().getCompound("display").getList("Lore", 8);
		String trade = getUnformattedText(tagList.getString(1).split(": ")[1]);
		int level = Integer.valueOf(getUnformattedText(tagList.getString(2).split(": ")[1]));

		ListNBT lore = getOrCreateLore();
		lore.add(new StringNBT());
		lore.add(new StringNBT("\u00a7r\u00a72LyriaWerkbank"));
		lore.add(new StringNBT("\u00a75\u00a7oKlasse:\u00a7r\u00a77 " + trade));
		lore.add(new StringNBT("\u00a75\u00a7oLevel:\u00a7r\u00a77 " + String.valueOf(level)));

		return trade;
	}

	public void setDataConstant()
	{
		if(!item.equals(name))
		{
			ListNBT lore = getOrCreateLore();

			lore.add(new StringNBT());
			lore.add(new StringNBT("\u00a7r\u00a72LyriaWerkbank"));
			lore.add(new StringNBT("\u00a75\u00a7oItem kann nicht"));
			lore.add(new StringNBT("\u00a75\u00a7ohergestellt werden"));
		}
	}

	public void fixEnchantments()
	{
		if(nbt != null && nbt.contains("Enchantments"))
		{
			ListNBT enchat = nbt.getList("Enchantments", (new CompoundNBT()).getId());
			for(int i = 0; i < enchat.size(); i++)
			{
				CompoundNBT compound = enchat.getCompound(i);
				int level = compound.getInt("lvl");
				compound.remove("lvl");
				compound.putInt("lvl", level);
			}
		}
	}

	private ListNBT getOrCreateLore()
	{
		CompoundNBT display;
		if(nbt != null && nbt.contains("display"))
		{
			display = nbt.getCompound("display");
		}
		else
		{
			display = new CompoundNBT();
			nbt = new CompoundNBT();
			nbt.put("display", display);
		}
		ListNBT lore;
		if(display.contains("Lore"))
		{
			lore = display.getList("Lore", 8);
		}
		else
		{
			lore = new ListNBT();
			display.put("Lore", lore);
		}
		return lore;
	}

	public JsonObject toJsonRecipeIngredient()
	{
		return toJsonRecipe(true);
	}

	public JsonObject toJsonRecipeResult()
	{
		return toJsonRecipe(false);
	}

	private JsonObject toJsonRecipe(boolean type)
	{
		JsonObject obj = new JsonObject();
		boolean isMinecraft = item.equals(name);
		if(isMinecraft)
		{
			obj.addProperty("item", name);
		}
		else
		{
			obj.addProperty("constant", "lyriawerkbank:" + RecipeImporterWriter.normalizeName(name));
		}

		if(count != 1)
		{
			obj.addProperty("count", count);
			if(type)
			{
				obj.addProperty("type", isMinecraft ? "lyriawerkbank:count_ingredient" : "lyriawerkbank:count_constants_ingredient");
			}
		}
		if(isMinecraft && nbt != null)
		{
			obj.add("nbt", getJsonFromNBT(nbt));
		}

		return obj;
	}

	public JsonObject toJsonConstant(boolean override)
	{
		JsonObject ingredient = new JsonObject();
		ingredient.addProperty("type", "lyria_ingredient");
		ingredient.addProperty("item", item);
		if(nbt != null)
		{
			ingredient.add("nbt", getJsonFromNBT(nbt));
		}

		JsonObject obj = new JsonObject();
		obj.addProperty("name", RecipeImporterWriter.normalizeName(name));
		obj.add("item", ingredient);

		return obj;
	}

	public boolean equals(Object obj)
	{
		if(!(obj instanceof RecipeImporterItem))
		{
			return false;
		}
		RecipeImporterItem data = (RecipeImporterItem) obj;
		if(!name.equals(data.name) || !item.equals(data.item) || count != data.count)
		{
			return false;
		}
		if(nbt == null && data.nbt != null || nbt != null && !nbt.equals(data.nbt))
		{
			return false;
		}
		return true;
	}

	public String getName()
	{
		return name;
	}

	public String getItem()
	{
		return item;
	}

	private static String getUnformattedText(String text)
	{
		return TextFormatting.getTextWithoutFormattingCodes(text);
	}

	private JsonObject getJsonFromNBT(CompoundNBT tag)
	{
		return RecipeImporter.GSON.fromJson(tag.toString(), JsonObject.class);
	}
}
