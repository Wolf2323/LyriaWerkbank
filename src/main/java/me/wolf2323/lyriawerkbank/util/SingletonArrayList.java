package me.wolf2323.lyriawerkbank.util;

import java.util.ArrayList;


@SuppressWarnings("serial")
public class SingletonArrayList<E> extends ArrayList<E>
{
	@Override
	public boolean add(E e)
	{
		if(!contains(e))
		{
			return super.add(e);
		}
		return false;
	}
}
