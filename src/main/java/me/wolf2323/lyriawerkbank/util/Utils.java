package me.wolf2323.lyriawerkbank.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.util.Collections;


public class Utils
{
	public static void reflectSetFinal(Field field, Object instance, Object newValue) throws Exception
	{
		field.setAccessible(true);

		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(instance, newValue);
	}

	public static FileSystem getZipFileSystem(final URI uri) throws IOException
	{
		final URI nUri = "jar".equals(uri.getScheme()) ? uri : URI.create("jar:" + uri.toString());
		return getFileSystem(nUri);
	}

	private static FileSystem getFileSystem(final URI uri) throws IOException
	{
		try
		{
			return FileSystems.getFileSystem(uri);
		}
		catch(FileSystemNotFoundException e)
		{
			return FileSystems.newFileSystem(uri, Collections.emptyMap());
		}
	}
}
