ScrollLock::
Loop, 700
{
    if(A_Index < 0) {
        continue ;
    }
    
    Send, t
    sleep 100
    Send /cr vorschau %A_Index%
    Send {Enter}
    sleep 2000
    MouseGetPos, MouseX, MouseY
    PixelGetColor, color, %MouseX%, %MouseY%, RGB
    if(color = 0xDBE1E3){
        Send {ESC}
        sleep 100
    }
}
return

PrintScreen::
Loop, 1000
{
    if(A_Index < 700) {
        continue ;
    }
    
    Send, t
    sleep 100
    Send /cr vorschau %A_Index%
    Send {Enter}
    sleep 900
}
return

Pause::
ExitApp